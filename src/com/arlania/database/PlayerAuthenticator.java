package com.arlania.database;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerAuthenticator {
	private static final String dbConnectionLink = "jdbc:mysql://38.135.32.111:3306/venturia_forum?user=venturia_admin&password=Sedfred456852";
	
	public static boolean playerIsRegistered(String username){
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			PreparedStatement queryGivePlayerWithName = conn.prepareStatement("SELECT username FROM mybb_users WHERE username = '" + username + "'");
			try(ResultSet rs = queryGivePlayerWithName.executeQuery()){
				while(rs.next()){
					return true;
				}
			}
		} catch (SQLException e) {
			System.out.println("Connection failed, time out, make sure database is online.");
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean playerNameAndPasswordMatch(String username, String password){
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			PreparedStatement queryGivePlayerSalt = conn.prepareStatement("SELECT username, salt FROM mybb_users WHERE username = '" + username + "'");
			String salt = "";
			try(ResultSet rs = queryGivePlayerSalt.executeQuery()){
				while(rs.next()){
					if(username.equalsIgnoreCase(rs.getString(1))){
						salt = rs.getString(2);
					}
				}
			}
			String hashedPw = md5(md5(salt) + md5(password));
			PreparedStatement queryGivePlayerWithName = conn.prepareStatement("SELECT username, password FROM mybb_users WHERE username = '" + username + "'");
			try(ResultSet rs = queryGivePlayerWithName.executeQuery()){
				while(rs.next()){
					if(username.equalsIgnoreCase(rs.getString(1)) && hashedPw.equals(rs.getString(2))){
						return true;
					}
				}
			}
		} catch (SQLException e) {
			System.out.println("Can't create connection to database, connection timed out.");
		}
		return false;
	}
	
	private static String md5(String tekst){
		try{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(tekst.getBytes());
			StringBuilder builder = new StringBuilder();
			for(int i = 0; i < array.length; i++){
				builder.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return builder.toString();
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
}
