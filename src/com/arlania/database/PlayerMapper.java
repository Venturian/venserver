package com.arlania.database;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.arlania.GameServer;
import com.arlania.model.Skill;
import com.arlania.util.Stopwatch;
import com.arlania.world.World;
import com.arlania.world.entity.impl.player.Player;
import com.arlania.world.entity.impl.player.PlayerLoadingForPlayerFilesChange;

public class PlayerMapper implements Runnable { 
	private final String dbConnectionLink = "jdbc:mysql://38.135.32.111:3306/venturia_highscores?user=venturia_admin&password=Sedfred456852";
	
	private void updateDb(){
		GameServer.getLogger().info("Updating Highscores!");
		List<String> dbLijst = givePlayersAllreadyInDb();
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			List<Player> lijst = giveAllPlayers();
			StringBuilder builder;
			for(Player p : lijst){
				if(dbLijst.contains(p.getUsername())){
					builder = new StringBuilder();
					builder.append("UPDATE venturia_highscores SET").append(" rights = ?, online = ?, donated = ?");
					for(int i = 0; i < Skill.values().length; i++) {
						builder.append(", " + Skill.values()[i].name().toLowerCase() + "_xp = ?").append(", " + Skill.values()[i].name().toLowerCase() + "_prestige = ?");
					}
					builder.append(", pkpoints = ?, pkkills = ?, pkdeaths =?, killstreak = ?, game_mode = ? WHERE username = ?");
					
					int nextPos = 1;
					
					PreparedStatement queryUpdatePlayer = conn.prepareStatement(builder.toString());
					queryUpdatePlayer.setString(nextPos++, p.getRights().toString());
					Player online = World.getPlayerByName(p.getUsername());
					if(online != null){
						queryUpdatePlayer.setString(nextPos++, "online");
					}else{
						queryUpdatePlayer.setString(nextPos++, "offline");
					}
					queryUpdatePlayer.setInt(nextPos++, p.getAmountDonated());
					
					for(int i = 0; i < Skill.values().length; i++) {
						queryUpdatePlayer.setInt(nextPos++, p.getSkillManager().getExperience(Skill.values()[i]));
						queryUpdatePlayer.setInt(nextPos++, p.getSkillManager().getCurrentPrestige(Skill.values()[i]));
					}
					
					queryUpdatePlayer.setInt(nextPos++, p.getPointsHandler().getPkPoints());
					queryUpdatePlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerKills());
					queryUpdatePlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerDeaths());
					queryUpdatePlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerKillStreak());
					queryUpdatePlayer.setString(nextPos++, p.getGameMode().name().replace("_", " ").toLowerCase());
					queryUpdatePlayer.setString(nextPos++, p.getUsername());
					queryUpdatePlayer.executeUpdate();
					
				}else{
					builder = new StringBuilder();
					builder.append("INSERT INTO venturia_highscores").append(" (username, rights, online, donated");
					for(int i = 0; i < Skill.values().length; i++) {
						builder.append(", " + Skill.values()[i].name().toLowerCase() + "_xp").append(", " + Skill.values()[i].name().toLowerCase() + "_prestige");
					}
					builder.append(", pkpoints, pkkills, pkdeaths, killstreak, game_mode) VALUES (?, ?, ?, ?");
					for(int i = 0; i < Skill.values().length; i++) {
						builder.append(", ?, ?");
					}
					builder.append(", ?, ?, ?, ?, ?)");
					
					PreparedStatement queryInsertPlayer = conn.prepareStatement(builder.toString());
					
					int nextPos = 1;
					queryInsertPlayer.setString(nextPos++, p.getUsername());
					queryInsertPlayer.setString(nextPos++, p.getRights().toString());
					Player online = World.getPlayerByName(p.getUsername());
					if(online != null){
						queryInsertPlayer.setString(nextPos++, "online");
					}else{
						queryInsertPlayer.setString(nextPos++, "offline");
					}
					queryInsertPlayer.setInt(nextPos++, p.getAmountDonated());
					for(int i = 0; i < Skill.values().length; i++) {
						queryInsertPlayer.setInt(nextPos++, p.getSkillManager().getExperience(Skill.values()[i]));
						queryInsertPlayer.setInt(nextPos++, p.getSkillManager().getCurrentPrestige(Skill.values()[i]));
					}
					
					queryInsertPlayer.setInt(nextPos++, p.getPointsHandler().getPkPoints());
					queryInsertPlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerKills());
					queryInsertPlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerDeaths());
					queryInsertPlayer.setInt(nextPos++, p.getPlayerKillingAttributes().getPlayerKillStreak());
					queryInsertPlayer.setString(nextPos++, p.getGameMode().name().replace("_", " ").toLowerCase());
					queryInsertPlayer.executeUpdate();
				}
			}
		}catch(SQLException e){
			for(Throwable t : e){
				t.printStackTrace();
			}
		}
	}
	
	private List<String> givePlayersAllreadyInDb(){
		List<String> lijst = new ArrayList<>();
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			PreparedStatement queryGivePlayerWithName = conn.prepareStatement("SELECT username FROM venturia_highscores");
			try(ResultSet rs = queryGivePlayerWithName.executeQuery()){
				while(rs.next()){
					lijst.add(rs.getString(1));
				}
			}
		} catch (SQLException e) {
			for(Throwable t : e){
				t.printStackTrace();
			}
		}
		return lijst;
	}
	
	private List<Player> giveAllPlayers(){
		Path path = Paths.get("./data/saves/characters");
		File folder = path.toFile();

		List<Player> lijst = new ArrayList<>();
		
		for(File file : folder.listFiles()){
			if(file.getName().substring(file.getName().length()-4, file.getName().length()).equals("json")){
				Player p = new Player(null);
				p.setUsername(file.getName().substring(0, file.getName().length()-5));
				PlayerLoadingForPlayerFilesChange.getResult(p);
				lijst.add(p);
			}
		}
		
		return lijst;
	}

	@Override
	public void run() {
		updateDb();
	}
}
