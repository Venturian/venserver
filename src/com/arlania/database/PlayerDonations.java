package com.arlania.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.arlania.customhelpclasses.ItemDropHelper;
import com.arlania.model.Item;

public class PlayerDonations {
	private static final String dbConnectionLink = "jdbc:mysql://38.135.32.111:3306/venturia_forum?user=venturia_admin&password=Sedfred456852";
	
	public static ItemDropHelper giveItems(String username){
		ItemDropHelper helper = new ItemDropHelper();
		
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			PreparedStatement queryGiveDonatedItems = conn.prepareStatement("SELECT username, itemid, amount FROM donationitems WHERE username = '" + username +"'");
			try(ResultSet rs = queryGiveDonatedItems.executeQuery()){
				while(rs.next()){
					if(rs.getInt(3) != 0){
						helper.addToLijst(new Item(rs.getInt(2), rs.getInt(3)));
					}
				}
			}
		} catch (SQLException e) {
			for(Throwable t : e){
				t.printStackTrace();
			}
		}
		
		
		return helper;
	}
	
	public static void updateItemsAfterClaim(String username, ItemDropHelper nieuweHoeveelheden){
		try(Connection conn = DriverManager.getConnection(dbConnectionLink)){
			PreparedStatement queryUpdateAmounts = conn.prepareStatement("UPDATE donationitems SET amount = ? WHERE username = ? AND itemid = ?");
			PreparedStatement queryDeleteRecord = conn.prepareStatement("DELETE FROM donationitems WHERE username = ? AND itemid = ?");
			for(Item item : nieuweHoeveelheden.getLijst()){
				if(item.getAmount() == 0){
					queryDeleteRecord.setString(1, username);
					queryDeleteRecord.setInt(2, item.getId());
					queryDeleteRecord.executeUpdate();
				}else{
					queryUpdateAmounts.setInt(1, item.getAmount());
					queryUpdateAmounts.setString(2, username);
					queryUpdateAmounts.setInt(3, item.getId());
					queryUpdateAmounts.executeUpdate();
				}
			}
		} catch (SQLException e) {
			for(Throwable t : e){
				t.printStackTrace();
			}
		}
	}
}
