package com.arlania.database;

import com.arlania.util.Stopwatch;

public class PlayerMapperTimer{
	private static final int TIME = 300000;
	private static Stopwatch timer = new Stopwatch().reset();
	
	public static void sequence(){
		if(timer.elapsed(TIME)){
			timer.reset();
			Thread t = new Thread(new PlayerMapper());
			t.start();
		}
	}
}