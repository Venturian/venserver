package com.arlania.customhelpclasses.streamHelpers;

import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface StreamIterator<T> extends Iterable<T> {

    @Override
    Iterator<T> iterator();

    default Stream<T> stream(){
        return StreamSupport.stream(this.spliterator(), false);
    }
}
