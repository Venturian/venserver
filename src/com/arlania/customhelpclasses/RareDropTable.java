package com.arlania.customhelpclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.arlania.model.Item;

public class RareDropTable {
	private static final List<RareDropItem> lijst = new ArrayList<>();
	private static final Random random = new Random();
	private static final int minimumLvlMonster = 60;
	
	static {
		lijst.add(new RareDropItem(995, 5000, 300000));
		lijst.add(new RareDropItem(200, 1, 20));
		lijst.add(new RareDropItem(202, 1, 20));
		lijst.add(new RareDropItem(204, 1, 20));
		lijst.add(new RareDropItem(206, 1, 20));
		lijst.add(new RareDropItem(208, 1, 20));
		lijst.add(new RareDropItem(210, 1, 20));
		lijst.add(new RareDropItem(212, 1, 20));
		lijst.add(new RareDropItem(214, 1, 20));
		lijst.add(new RareDropItem(216, 1, 20));
		lijst.add(new RareDropItem(218, 1, 20));
		lijst.add(new RareDropItem(220, 1, 20));
		lijst.add(new RareDropItem(2486, 1, 20));
		lijst.add(new RareDropItem(3050, 1, 20));
		lijst.add(new RareDropItem(3052, 1, 20));
		lijst.add(new RareDropItem(12175, 1, 20));
		lijst.add(new RareDropItem(987, 1, 1));
		lijst.add(new RareDropItem(985, 1, 1));
		lijst.add(new RareDropItem(1622, 1, 5));
		lijst.add(new RareDropItem(1624, 1, 5));
		lijst.add(new RareDropItem(1618, 1, 5));
		lijst.add(new RareDropItem(1620, 1, 5));
		lijst.add(new RareDropItem(1632, 1, 5));
		lijst.add(new RareDropItem(1452, 1, 1));
		lijst.add(new RareDropItem(1462, 1, 1));
		lijst.add(new RareDropItem(830, 5, 5));
		lijst.add(new RareDropItem(533, 1, 82));
		lijst.add(new RareDropItem(8781, 1, 55));
		lijst.add(new RareDropItem(44, 1, 230));
		lijst.add(new RareDropItem(1149, 1, 1));
		lijst.add(new RareDropItem(1127, 1, 1));
		lijst.add(new RareDropItem(1128, 1, 25));
		lijst.add(new RareDropItem(2362, 1, 30));
		lijst.add(new RareDropItem(2364, 1, 20));
		lijst.add(new RareDropItem(2360, 1, 40));
		lijst.add(new RareDropItem(1618, 1, 60));
		lijst.add(new RareDropItem(1619, 1, 30));
		lijst.add(new RareDropItem(1632, 1, 20));
		lijst.add(new RareDropItem(1516, 1, 40));
		lijst.add(new RareDropItem(1514, 1, 20));
		lijst.add(new RareDropItem(1520, 1, 80));
		lijst.add(new RareDropItem(445, 1, 75));
		lijst.add(new RareDropItem(448, 1, 50));
		lijst.add(new RareDropItem(452, 1, 30));
		lijst.add(new RareDropItem(378, 1, 100));
		lijst.add(new RareDropItem(7945, 1, 50));
		lijst.add(new RareDropItem(384, 1, 50));
		lijst.add(new RareDropItem(1305, 1, 1));
		lijst.add(new RareDropItem(18831, 1, 30));
		lijst.add(new RareDropItem(2528, 1, 1));
		lijst.add(new RareDropItem(18782, 1, 1));
		lijst.add(new RareDropItem(2, 1, 150));
		lijst.add(new RareDropItem(6, 1, 1));
		lijst.add(new RareDropItem(8, 1, 1));
		lijst.add(new RareDropItem(10, 1, 1));
		lijst.add(new RareDropItem(12, 1, 1));
	}
	
	public static Item geefRareDrop(int monsterLvl) {
		if (monsterLvl >= minimumLvlMonster && checkOfItemDropMetKans(monsterLvl)) {
			RareDropItem i = lijst.get(random.nextInt(lijst.size()));
			int aantal = random.nextInt(i.getMaxAantal() - i.getMinAantal() + 1) + i.getMinAantal();
			return new Item(i.getId(), aantal);
		}
		return null;
	}
	
	private static boolean checkOfItemDropMetKans(int monsterLvl){
		int omgekeerdeKans;
		if(monsterLvl <= 90) {
			omgekeerdeKans = 100;
		}else if (monsterLvl <= 110) {
			omgekeerdeKans = 50;
		}else if (monsterLvl <= 250) {
			omgekeerdeKans = 20;
		}else {
			omgekeerdeKans = 6;
		}
        return random.nextInt(omgekeerdeKans) == 2;
    }
	
	private static class RareDropItem{
		private int id;
		private int minAantal;
		private int maxAantal;
		
		public RareDropItem(int id, int min, int max) {
			this.id = id;
			this.minAantal = min;
			this.maxAantal = max;
		}
		
		public int getId() {
			return id;
		}
		
		public int getMaxAantal() {
			return maxAantal;
		}
		
		public int getMinAantal() {
			return minAantal;
		}
	}
}
