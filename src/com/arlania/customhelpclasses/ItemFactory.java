package com.arlania.customhelpclasses;

public class ItemFactory {

    private GameItem item;

    public ItemFactory() {
        this.item = new GameItem();
    }

    public ItemFactory setId(final int itemId){
        this.item.setItemId(itemId);
        return this;
    }

    public ItemFactory setValue(final int value){
        this.item.setValue(value);
        return this;
    }

    public ItemFactory setBonuses(final double... bonuses){
        this.item.setBonuses(bonuses);
        return this;
    }

    public ItemFactory setRequirements(final int... requirements){
        this.item.setRequirements(requirements);
        return this;
    }

    public ItemFactory setStackable(boolean stackable){
        this.item.setStackable(stackable);
        return this;
    }

    public ItemFactory setIsNote(boolean isNote){
        this.item.setNote(isNote);
        return this;
    }

    public ItemFactory setTwoHand(boolean isTwoHand){
        this.item.setTwoHanded(isTwoHand);
        return this;
    }

    public ItemFactory setIsWeapon(boolean isWeapon){
        this.item.setWeapon(isWeapon);
        return this;
    }

    public ItemFactory setName(final String name){
        this.item.setName(name);
        return this;
    }

    public ItemFactory setEquipSlot(final int slot){
        this.item.setEquipSlot(slot);
        return this;
    }

    public static String getBonusName(final int index) {
        switch (index) {
            case 0:
                return "Stab";
            case 1:
                return "Slash";
            case 2:
                return "Crush";
            case 3:
                return "Magic";
            case 4:
                return "Range";
            case 5:
                return "D - Stab";
            case 6:
                return "D - Slash";
            case 7:
                return "D - Crush";
            case 8:
                return "D - Magic";
            case 9:
                return "D - Range";
            case 10:
                return "Summoning";
            case 11:
                return "Absorb Melee";
            case 12:
                return "Absorb Magic";
            case 13:
                return "Absorb Ranged";
            case 14:
                return "Strength";
            case 15:
                return "Ranged Strength";
            case 16:
                return "Prayer";
            case 17:
                return "Magic Damage";
        }
        return "";
    }

    public GameItem createItem(){
        return new GameItem(this.item);
    }

	public ItemFactory setDesc(String desc) {
		this.item.setDesc(desc);
		return this;
	}
}
