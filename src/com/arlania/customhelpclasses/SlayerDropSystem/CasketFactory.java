package com.arlania.customhelpclasses.SlayerDropSystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.arlania.model.Item;
import com.arlania.world.entity.impl.player.Player;

public class CasketFactory {
	private static final Casket low = new LowCasket();
	private static final Casket mid = new MidCasket();
	private static final Casket high = new HighCasket();
	private static final Random rd = new Random();
	private static final int omgekeerdeKans = 50;
	
	public static boolean isCasket(int id) {
		return id == low.getId() || id == mid.getId() || id == high.getId();
	}
	
	public static boolean checkOfCasketDropt() {
		int getal = rd.nextInt(omgekeerdeKans);
		System.out.println(getal);
		return getal == 2;
	}
	
	public static Casket geefCasket(int monsterLvl) {
		if(monsterLvl <= 50) {
			return low;
		}else if(monsterLvl <= 100) {
			return mid;
		}else {
			return high;
		}
	}
	
	private static Casket geefCaksetMetId(int id) {
		if(id == low.getId()) {
			return low;
		}else if(id == mid.getId()) {
			return mid;
		}else if(id == high.getId()) {
			return high;
		}else {
			return null;
		}
	}
	
	public static void geefReward(Player p, int id) {
		if(p.getInventory().getFreeSlots() <= 0) {
			p.getPacketSender().sendMessage("You do not have enough free inventory slots to do this.");
			return;
		}
		Casket c = geefCaksetMetId(id);
		p.getInventory().delete(c.getId(), 1);
		p.getInventory().add(c.geefReward());
	}
	
	private static class LowCasket implements Casket{
		private final int id;
		private final List<Item> lijst;
		
		public LowCasket() {
			id = 13064;
			lijst = new ArrayList<>();
			lijst.add(new Item(1, 13064));
		}

		@Override
		public Item geefReward() {
			return lijst.get(rd.nextInt(lijst.size()));
		}

		@Override
		public int getId() {
			return id;
		}
	}
	
	private static class MidCasket implements Casket{
		private final int id;
		private final List<Item> lijst;
		
		public MidCasket() {
			id = 13066;
			lijst = new ArrayList<>();
			lijst.add(new Item(1, 13066));
		}

		@Override
		public Item geefReward() {
			return lijst.get(rd.nextInt(lijst.size()));
		}
		
		@Override
		public int getId() {
			return id;
		}
	}
	

	private static class HighCasket implements Casket{
		private final int id;
		private final List<Item> lijst;
		
		public HighCasket() {
			id = 13077;
			lijst = new ArrayList<>();
			lijst.add(new Item(1, 13077));	
		}

		@Override
		public Item geefReward() {
			return lijst.get(rd.nextInt(lijst.size()));
		}
		
		@Override
		public int getId() {
			return id;
		}
	}
}

