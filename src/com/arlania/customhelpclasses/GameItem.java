package com.arlania.customhelpclasses;

import java.util.Arrays;

public class GameItem {

    private String name;
    private String desc;
    private int itemId;
    private int value;
    private int equipSlot;
    private double[] bonuses;
    private int[] requirements;
    private boolean stackable;
    private boolean isNote;
    private boolean isWeapon;
    private boolean isTwoHanded;


    GameItem(){}

    public GameItem(GameItem gameItem){
        this.itemId = gameItem.itemId;
        this.value = gameItem.value;
        this.bonuses = new double[gameItem.bonuses.length];
        System.arraycopy(gameItem.bonuses, 0, this.bonuses, 0, gameItem.bonuses.length);
        this.requirements = new int[gameItem.requirements.length];
        System.arraycopy(gameItem.requirements, 0, this.requirements, 0, gameItem.requirements.length);
        this.stackable = gameItem.stackable;
        this.isNote = gameItem.isNote;
        this.isTwoHanded = gameItem.isTwoHanded;
        this.isWeapon = gameItem.isWeapon;
        this.name = gameItem.name;
        this.desc = gameItem.desc;
        this.equipSlot = gameItem.equipSlot;
    }

    @Override
    public String toString() {
        return "GameItem{" +
                "itemId=" + itemId +
                ", value=" + value +
                ", bonuses=" + Arrays.toString(bonuses) +
                ", requirements=" + Arrays.toString(requirements) +
                '}';
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public double[] getBonuses() {
        return bonuses;
    }

    public void setBonuses(double[] bonuses) {
        this.bonuses = bonuses;
    }

    public int[] getRequirement() {
        return requirements;
    }

    public void setRequirements(int[] requirements) {
        this.requirements = requirements;
    }

    public boolean isStackable() {
        return stackable;
    }

    public void setStackable(boolean stackable) {
        this.stackable = stackable;
    }

    public boolean isNote() {
        return isNote;
    }

    public void setNote(boolean note) {
        isNote = note;
    }

    public boolean isWeapon() {
        return isWeapon;
    }

    public void setWeapon(boolean weapon) {
        isWeapon = weapon;
    }

    public boolean isTwoHanded() {
        return isTwoHanded;
    }

    public void setTwoHanded(boolean twoHanded) {
        isTwoHanded = twoHanded;
    }

    public String getName() {
        return name;
    }
    
    public String getDesc() {
    	return this.desc;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getEquipSlot() {
        return equipSlot;
    }

    public void setEquipSlot(int equipSlot) {
        this.equipSlot = equipSlot;
    }

	public void setDesc(String desc) {
		this.desc = desc;
	}
}
