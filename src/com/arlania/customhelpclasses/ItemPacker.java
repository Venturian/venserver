package com.arlania.customhelpclasses;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public final class ItemPacker {

    public static List<GameItem> gameItems = new ArrayList<>();

    public static void readItems() {
        try {
            final byte[] buffer = Files.readAllBytes(new File("data/def/dat/gameItems.dat").toPath());
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(buffer));
            ItemFactory factory = new ItemFactory();
            do {
                int opCode = in.readByte();
                if (opCode == 0) {
                    break;
                }
                if (opCode == 1) {
                    final int id = in.readInt();
                    final int value = in.readInt();
                    final int byteV = in.readByte();
                    final byte byteValue = (byte) (byteV >> 4);
                    final boolean isNote = checkBit(byteValue, 0);
                    final boolean isStackable = checkBit(byteValue, 1);
                    final boolean isTwoHanded = checkBit(byteValue, 2);
                    final boolean isWeapon = checkBit(byteValue, 3);
                    final int slot = (byteV & 0xf);

                    final int reqSize = in.readByte();
                    final int[] reqs = new int[reqSize];
                    for (int i = 0; i < reqSize; i++) {
                        reqs[i] = in.readByte();
                    }
                    final int bonusSize = in.readByte();
                    final double[] bonuses = new double[bonusSize];
                    for (int j = 0; j < bonusSize; j++) {
                        bonuses[j] = in.readShort();
                    }
                    final String name = in.readUTF();
                    final String desc = in.readUTF();
                    GameItem g = factory.setId(id)
                            .setValue(value)
                            .setIsWeapon(isWeapon)
                            .setStackable(isStackable)
                            .setTwoHand(isTwoHanded)
                            .setIsNote(isNote)
                            .setEquipSlot(slot)
                            .setRequirements(reqs)
                            .setBonuses(bonuses)
                            .setName(name)
                            .setDesc(desc)
                            .createItem();
                    ItemPacker.gameItems.add(g);
                    factory = new ItemFactory();
                }
            } while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean checkBit(int value, int pos){
        int shifted = value >> pos;
        return (shifted & 1) == 1;
    }

}
