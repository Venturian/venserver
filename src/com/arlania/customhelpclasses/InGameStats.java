package com.arlania.customhelpclasses;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.arlania.world.content.Scoreboards.Scoreboard;
import com.arlania.world.content.Scoreboards.ScoreboardEntry;
import com.arlania.model.PlayerRights;
import com.arlania.world.World;
import com.arlania.world.content.Scoreboards;
import com.arlania.world.content.StaffList;
import com.arlania.world.entity.impl.player.Player;
import com.arlania.world.entity.impl.player.PlayerLoadingForPlayerFilesChange;

public class InGameStats implements Runnable{
	private static List<ScoreboardEntry> topKillStreak = new ArrayList<>();
	private static List<ScoreboardEntry> topPkers = new ArrayList<>();
	private static List<ScoreboardEntry> topTotalExp = new ArrayList<>();
	private static List<ScoreboardEntry> topAchiever = new ArrayList<>();

	public static void init(){
		List<Player> players = new ArrayList<>();
		
		Path path = Paths.get("./data/saves/characters");
		File folder = path.toFile();

		for(File file : folder.listFiles()){
			if(file.getName().substring(file.getName().length()-4, file.getName().length()).equals("json")){
				Player p = new Player(null);
				p.setUsername(file.getName().substring(0, file.getName().length()-5));
				PlayerLoadingForPlayerFilesChange.getResult(p);
				players.add(p);
			}
		}
		
		fillStaffList(players);
		fillScoreboard(players);
	}
	
	private static void update(){
		List<Player> players = new ArrayList<>();
		
		Path path = Paths.get("./data/saves/characters");
		File folder = path.toFile();

		for(File file : folder.listFiles()){
			if(file.getName().substring(file.getName().length()-4, file.getName().length()).equals("json")){
				Player p = new Player(null);
				p.setUsername(file.getName().substring(0, file.getName().length()-5));
				PlayerLoadingForPlayerFilesChange.getResult(p);
				players.add(p);
			}
		}
		topKillStreak = new ArrayList<>();
		topAchiever = new ArrayList<>();
		topPkers = new ArrayList<>();
		topTotalExp = new ArrayList<>();
		fillScoreboard(players);
	}
	
	private static void fillStaffList(List<Player> players){
		players.stream().filter(p -> p.getRights().isStaff()).collect(Collectors.toList()).forEach(p -> StaffList.logout(p));
	}
	
	private static void fillScoreboard(List<Player> players){
		List<Player> temp = players.stream().filter(p -> p.getRights() != PlayerRights.OWNER && p.getRights() != PlayerRights.DEVELOPER).collect(Collectors.toList());
		
		for(Player p : temp) {
			if(p == null)
				continue;
			//top pkers
			int kc = p.getPlayerKillingAttributes().getPlayerKills();
			int dc = p.getPlayerKillingAttributes().getPlayerDeaths();
			double kdr = (double) ((kc < 2 || dc < 2) ? (0) : (kc/dc));
			topPkers.add(new ScoreboardEntry(p.getUsername(), new String[]{String.valueOf(kdr), String.valueOf(kc), String.valueOf(dc)}));
			//top killstreaks
			topKillStreak.add(new ScoreboardEntry(p.getUsername(), new String[]{String.valueOf(p.getPlayerKillingAttributes().getPlayerKillStreak())}));
			//top totalexp
			topTotalExp.add(new ScoreboardEntry(p.getUsername(), new String[]{String.valueOf(p.getSkillManager().getTotalExp())}));
			//top achiever
			topAchiever.add(new ScoreboardEntry(p.getUsername(), new String[]{String.valueOf(p.getPointsHandler().getAchievementPoints())}));
		}
	}
	
	public static List<ScoreboardEntry> getList(Scoreboard scoreboard){
		if(scoreboard == Scoreboards.TOP_PKERS) {
			return new ArrayList<ScoreboardEntry>(topPkers);
		} else if(scoreboard == Scoreboards.TOP_KILLSTREAKS) {
			return new ArrayList<ScoreboardEntry>(topKillStreak);
		} else if(scoreboard == Scoreboards.TOP_TOTAL_EXP) {
			return new ArrayList<ScoreboardEntry>(topTotalExp);
		} else if(scoreboard == Scoreboards.TOP_ACHIEVER) {
			return new ArrayList<ScoreboardEntry>(topAchiever);
		}
		return new ArrayList<ScoreboardEntry>();
	}

	@Override
	public void run() {
		update();
		
	}
}
