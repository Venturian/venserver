package com.arlania.customhelpclasses;

import java.util.Random;

public class CharmDrop {
	private static int goldCharm = 12158;
	private static int greenCharm = 12159;
	private static int crimsonCharm = 12160;
	private static int blueCharm = 12163;
	
	private static final Random random = new Random();
	
	public static boolean checkOfEnWelkeCharmDropMetKans(int monsterLevel) {
		int kans;
		
		if(monsterLevel <= 30) {
			kans = 70;
		}else if (monsterLevel <= 60) {
			kans = 60;
		}else if (monsterLevel <= 90) {
			kans = 45;
		}else if (monsterLevel <= 120) {
			kans = 30;
		}else {
			kans = 15;
		}
        return random.nextInt(kans) == 8;
    }
	
	public static int kiesCharm() {
		int charm;
		int randomGetal = random.nextInt(19) +1;
		
		if(randomGetal <= 8) {
			charm = goldCharm;
		} else if (randomGetal >= 9 && randomGetal <= 14) {
			charm = greenCharm;
		} else if (randomGetal >= 15 && randomGetal <= 18) {
			charm = crimsonCharm;
		} else {
			charm = blueCharm;
		}
		
		return charm;
	}
}
