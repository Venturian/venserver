package com.arlania.customhelpclasses;

import java.util.ArrayList;
import java.util.List;

import com.arlania.model.Item;

public class ItemDropHelper {
	private int aantalRareDrops;
	private List<Item> lijst;
	
	public ItemDropHelper() {
		lijst = new ArrayList<>();
		aantalRareDrops = 0;
	}
	public List<Item> getLijst() {
		return lijst;
	}
	
	public void addToLijst(Item item){
		lijst.add(item);
	}
	
	public int getAantalRareDrops() {
		return aantalRareDrops;
	}
	
	public void verhoogAantalRareDrops() {
		aantalRareDrops++;
	}
}
