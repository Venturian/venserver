package com.arlania.model.container;

import com.arlania.model.Item;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ContainerIterator implements Iterator<Item> {

    private Item[] items;
    private int currentPosition;

    public ContainerIterator(Item[] items) {
        this.items = items;
        this.currentPosition = 0;
    }

    @Override
    public boolean hasNext() {
        return this.currentPosition < items.length;
    }

    @Override
    public Item next() {
        if(!hasNext()){
            throw new NoSuchElementException("No Element found in container iterator.");
        }
        return this.items[this.currentPosition++];
    }
}
