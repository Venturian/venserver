package com.arlania.model.input.impl;

import com.arlania.model.input.EnterAmount;
import com.arlania.world.content.skill.impl.crafting.Gems2;
import com.arlania.world.entity.impl.player.Player;

public class EnterAmountOfTipsToMake extends EnterAmount {

	@Override
	public void handleAmount(Player player, int amount) {
		if(player.getSelectedSkillingItem() > 0)
			Gems2.cutGem(player, amount, player.getSelectedSkillingItem());
	}

}