package com.arlania.model;

import com.arlania.util.Misc;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;


/**
 * Represents a player's privilege rights.
 * @author Gabriel Hannason
 */

public enum PlayerRights {

	/*
	 * A regular member of the server.
	 */
	PLAYER(-1, null, 1, 1, "PLAYER"),
	/*
	 * A moderator who has more privilege than other regular members and donators.
	 */
	MODERATOR_NORMAL(-1, "<col=20B2AA>", 1, 1, "MODERATOR"),
	MODERATOR_DONATOR(-1, "<col=20B2AA>", 1.5, 1.05, "MODERATOR"),
	MODERATOR_SUPER_DONATOR(-1, "<col=20B2AA>", 1.5, 1.1, "MODERATOR"),
	MODERATOR_EXTREME_DONATOR(-1, "<col=20B2AA>", 2, 1.15, "MODERATOR"),
	MODERATOR_LEGENDARY_DONATOR(-1, "<col=20B2AA>", 2.20, 1.4, "MODERATOR"),
	MODERATOR_UBER_DONATOR(-1, "<col=20B2AA>", 3, 1.25, "MODERATOR"),

	/*
	 * The second-highest-privileged member of the server.
	 */
	ADMINISTRATOR_NORMAL(-1, "<col=FFFF64>", 1, 1, "ADMINISTRATOR"),
	ADMINISTRATOR_DONATOR(-1, "<col=FFFF64>", 1.5, 1.05, "ADMINISTRATOR"),
	ADMINISTRATOR_SUPER_DONATOR(-1, "<col=FFFF64>", 1.5, 1.1, "ADMINISTRATOR"),
	ADMINISTRATOR_EXTREME_DONATOR(-1, "<col=FFFF64>", 2, 1.15, "ADMINISTRATOR"),
	ADMINISTRATOR_LEGENDARY_DONATOR(-1, "<col=FFFF64>", 2.5, 1.2, "ADMINISTRATOR"),
	ADMINISTRATOR_UBER_DONATOR(-1, "<col=FFFF64>", 3, 1.25, "ADMINISTRATOR"),

	/*
	 * The highest-privileged member of the server
	 */
	OWNER(-1, "<col=B40404>", 1, 1, "OWNER"),

	/*
	 * The Developer of the server, has same rights as the owner.
	 */
	DEVELOPER(-1, "<col=fa0505>", 1, 1, "DEVELOPER"),


	/*
	 * A member who has donated to the server. 
	 */
	DONATOR(60, "<shad=FF7F00>", 1.5, 1.05, "DONATOR"),
	SUPER_DONATOR(40, "<col=787878>", 1.5, 1.1, "SUPER_DONATOR"),
 	EXTREME_DONATOR(20, "<col=D9D919>", 2, 1.15, "EXTREME_DONATOR"),
	LEGENDARY_DONATOR(10, "<shad=697998>", 2.5, 1.2, "LEGENDARY_DONATOR"),
	UBER_DONATOR(0, "<col=0EBFE9>", 3, 1.25, "UBER_DONATOR"),

	/*
	 * A member who has the ability to help people better.
	 */
	SUPPORT_NORMAL(-1, "@blu@", 1, 1, "SUPPORT"),
	SUPPORT_DONATOR(-1, "@blu@", 1.5, 1.05, "SUPPORT"),
	SUPPORT_SUPER_DONATOR(-1, "@blu@", 1.5, 1.1, "SUPPORT"),
	SUPPORT_EXTREME_DONATOR(-1, "@blu@", 2, 1.15, "SUPPORT"),
	SUPPORT_LEGENDARY_DONATOR(-1, "@blu@", 2.5, 1.2, "SUPPORT"),
	SUPPORT_UBER_DONATOR(-1, "@blu@", 3, 1.25, "SUPPORT"),

	/*
	 * A member who has been with the server for a long time.
	 */
	YOUTUBER_NORMAL(30, "<col=CD661D>", 1, 1, "YOUTUBER"),
	YOUTUBER_DONATOR(30, "<col=CD661D>", 1.5, 1.05, "YOUTUBER"),
	YOUTUBER_SUPER_DONATOR(30, "<col=CD661D>", 1.5, 1.1, "YOUTUBER"),
	YOUTUBER_EXTREME_DONATOR(20, "<col=CD661D>", 2, 1.15, "YOUTUBER"),
	YOUTUBER_LEGENDARY_DONATOR(10, "<col=CD661D>", 2.5, 1.2, "YOUTUBER"),
	YOUTUBER_UBER_DONATOR(0, "<col=CD661D>", 3, 1.25, "YOUTUBER");

	PlayerRights(int yellDelaySeconds, String yellHexColorPrefix, double loyaltyPointsGainModifier, double experienceGainModifier, String showRank) {
		this.yellDelay = yellDelaySeconds;
		this.yellHexColorPrefix = yellHexColorPrefix;
		this.loyaltyPointsGainModifier = loyaltyPointsGainModifier;
		this.experienceGainModifier = experienceGainModifier;
		this.showRank = showRank;
	}
	
	private static final ImmutableSet<PlayerRights> STAFF = Sets.immutableEnumSet(SUPPORT_NORMAL, SUPPORT_DONATOR, SUPPORT_SUPER_DONATOR, SUPPORT_EXTREME_DONATOR, SUPPORT_LEGENDARY_DONATOR, SUPPORT_UBER_DONATOR, MODERATOR_NORMAL, MODERATOR_DONATOR, MODERATOR_SUPER_DONATOR, MODERATOR_EXTREME_DONATOR, MODERATOR_LEGENDARY_DONATOR, MODERATOR_UBER_DONATOR, ADMINISTRATOR_NORMAL, ADMINISTRATOR_DONATOR, ADMINISTRATOR_SUPER_DONATOR, ADMINISTRATOR_EXTREME_DONATOR, ADMINISTRATOR_LEGENDARY_DONATOR, ADMINISTRATOR_UBER_DONATOR, OWNER, DEVELOPER);
	private static final ImmutableSet<PlayerRights> MEMBERS = Sets.immutableEnumSet(DONATOR, SUPER_DONATOR, EXTREME_DONATOR, LEGENDARY_DONATOR, UBER_DONATOR, SUPPORT_DONATOR, SUPPORT_SUPER_DONATOR, SUPPORT_EXTREME_DONATOR, SUPPORT_LEGENDARY_DONATOR, SUPPORT_UBER_DONATOR, MODERATOR_DONATOR, MODERATOR_SUPER_DONATOR, MODERATOR_EXTREME_DONATOR, MODERATOR_LEGENDARY_DONATOR, MODERATOR_UBER_DONATOR,ADMINISTRATOR_DONATOR, ADMINISTRATOR_SUPER_DONATOR, ADMINISTRATOR_EXTREME_DONATOR, ADMINISTRATOR_LEGENDARY_DONATOR, ADMINISTRATOR_UBER_DONATOR);
	
	/*
	 * The yell delay for the rank
	 * The amount of seconds the player with the specified rank must wait before sending another yell message.
	 */
	private int yellDelay;
	private String yellHexColorPrefix;
	private double loyaltyPointsGainModifier;
	private double experienceGainModifier;
	private String showRank;
	
	public int getYellDelay() {
		return yellDelay;
	}
	
	public String getShowRank() {
		return showRank;
	}
	
	/*
	 * The player's yell message prefix.
	 * Color and shadowing.
	 */
	
	public String getYellPrefix() {
		return yellHexColorPrefix;
	}
	
	/**
	 * The amount of loyalty points the rank gain per 4 seconds
	 */
	public double getLoyaltyPointsGainModifier() {
		return loyaltyPointsGainModifier;
	}
	
	public double getExperienceGainModifier() {
		return experienceGainModifier;
	}
	
	public boolean isStaff() {
		return STAFF.contains(this);
	}
	
	public boolean isMember() {
		return MEMBERS.contains(this);
	}
	
	/**
	 * Gets the rank for a certain id.
	 * 
	 * @param id	The id (ordinal()) of the rank.
	 * @return		rights.
	 */
	public static PlayerRights forId(int id) {
		for (PlayerRights rights : PlayerRights.values()) {
			if (rights.ordinal() == id) {
				return rights;
			}
		}
		return null;
	}
	@Override
	public String toString() {
		return Misc.ucFirst(name().replaceAll("_", " "));
	}
}
