package com.arlania.world.content;

import com.arlania.util.Misc;
import com.arlania.util.Stopwatch;
import com.arlania.world.World;

/*
 * @author Bas
 * www.Arlania.com
 */

public class Reminders {
	
	
    private static final int TIME = 800000; //10 minutes
	private static Stopwatch timer = new Stopwatch().reset();
	public static String currentMessage;
	
	/*
	 * Random Message Data
	 */
	private static final String[][] MESSAGE_DATA = { 
			{"@blu@[INFO]@red@ Thank you for posting every bug found on ::forum"},
			{"@blu@[INFO]@red@ Use the command ::drop (npcname) for drop tables"},
			{"@blu@[INFO]@red@ Use the ::help command to ask staff for help"},
			{"@blu@[INFO]@red@ Visit www.Venturian.org for latest news & server updates"},
			{"@blu@[INFO]@red@ Make sure to ::vote twice a day and invite your friends to play!"},
			{"@blu@[INFO]@red@ Use ::commands to find a list of commands"},
			{"@blu@[INFO]@red@ Toggle your client settings to your preference in the wrench tab!"},
			{"@blu@[INFO]@red@ Register and post on the forums to keep them active! ::forum"},
			{"@blu@[INFO]@red@ Interested in making Youtube Video's ? Pm Alex or Jhon !"},
			
		
	};

	/*
	 * Sequence called in world.java
	 * Handles the main method
	 * Grabs random message and announces it
	 */
	public static void sequence() {
		if(timer.elapsed(TIME)) {
			timer.reset();
			{
				
			currentMessage = MESSAGE_DATA[Misc.getRandom(MESSAGE_DATA.length - 1)][0];
			World.sendMessage(currentMessage);
			World.savePlayers();
					
				}
				
			World.savePlayers();
			}
		

          }

}