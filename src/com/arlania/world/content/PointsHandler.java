package com.arlania.world.content;

import com.arlania.world.entity.impl.player.Player;

public class PointsHandler {

	private Player p;
	
	public PointsHandler(Player p) {
		this.p = p;
	}
	
	public void reset() {
		dungTokens = commendations = (int) (loyaltyPoints = votingPoints = slayerPoints = pkPoints = 0);
		p.getPlayerKillingAttributes().setPlayerKillStreak(0);
		p.getPlayerKillingAttributes().setPlayerKills(0);
		p.getPlayerKillingAttributes().setPlayerDeaths(0);
		p.getDueling().arenaStats[0] = p.getDueling().arenaStats[1] = 0;
	}

	public PointsHandler refreshPanel() {
		p.getPacketSender().sendString(26703, "@fz1@Trivia Points: @de0@"+triviaPoints);

		p.getPacketSender().sendString(26702, "@fz1@Prestige Points: @de0@"+prestigePoints);
		p.getPacketSender().sendString(26706, "@fz1@Commendations: @de0@ "+commendations);
		p.getPacketSender().sendString(26701, "@fz1@Loyalty Points: @de0@"+(int)loyaltyPoints);
		p.getPacketSender().sendString(26707, "@fz1@Bossing Points: @de0@ "+p.getBossPoints());

		p.getPacketSender().sendString(26710, "@fz1@Dung. Tokens: @de0@ "+dungTokens);
		p.getPacketSender().sendString(26704, "@fz1@Voting Points: @de0@ "+votingPoints);
		p.getPacketSender().sendString(26705, "@fz1@Slayer Points: @de0@"+slayerPoints);
		p.getPacketSender().sendString(26709, "@fz1@Pk Points: @de0@"+pkPoints);
		p.getPacketSender().sendString(26708, "@fz1@Donation Points: @de0@"+donationPoints);

		p.getPacketSender().sendString(26711, "@fz1@Wilderness Killstreak: @de0@"+p.getPlayerKillingAttributes().getPlayerKillStreak());
		p.getPacketSender().sendString(26712, "@fz1@Wilderness Kills: @de0@"+p.getPlayerKillingAttributes().getPlayerKills());		
		p.getPacketSender().sendString(26713, "@fz1@Wilderness Deaths: @de0@"+p.getPlayerKillingAttributes().getPlayerDeaths());
		p.getPacketSender().sendString(26714, "@fz1@Arena Victories: @de0@"+p.getDueling().arenaStats[0]);
		p.getPacketSender().sendString(26715, "@fz1@Arena Losses: @de0@"+p.getDueling().arenaStats[1]);
		PlayerPanel.refreshPanel(p);
		return this;
	}

	private int prestigePoints;
	private int triviaPoints;
	private int slayerPoints;
	private int commendations;
	private int dungTokens;
	private int pkPoints;
	private double loyaltyPoints;
	private int votingPoints;
	private int achievementPoints;
	private int donationPoints;
	
	public int getPrestigePoints() {
		return prestigePoints;
	}
	
	public void setPrestigePoints(int points, boolean add) {
		if(add)
			this.prestigePoints += points;
		else
			this.prestigePoints = points;
	}

	public int getSlayerPoints() {
		return slayerPoints;
	}
	
	public int getDonationPoints(){
		return donationPoints;
	}

	public void setDonationPoints(int donationPoints, boolean add) {
		if(add)
			this.donationPoints += donationPoints;
		else
			this.donationPoints = donationPoints;
		}
		public void incrementDonationPoints(double amount) {
		this.donationPoints += amount;
	}
		public int getTriviaPoints(){
			return triviaPoints;
		}

		public void setTriviaPoints(int triviaPoints, boolean add) {
			if(add)
				this.triviaPoints += triviaPoints;
			else
				this.triviaPoints = triviaPoints;
		}
			public void incrementTriviaPoints(double amount) {
			this.triviaPoints += amount;
		}
	public void setSlayerPoints(int slayerPoints, boolean add) {
		if(add)
			this.slayerPoints += slayerPoints;
		else
			this.slayerPoints = slayerPoints;
	}

	public int getCommendations() {
		return this.commendations;
	}

	public void setCommendations(int commendations, boolean add) {
		if(add)
			this.commendations += commendations;
		else
			this.commendations = commendations;
	}

	public int getLoyaltyPoints() {
		return (int)this.loyaltyPoints;
	}

	public void setLoyaltyPoints(int points, boolean add) {
		if(add)
			this.loyaltyPoints += points;
		else
			this.loyaltyPoints = points;
	}
	
	public void incrementLoyaltyPoints(double amount) {
		this.loyaltyPoints += amount;
	}
	
	public int getPkPoints() {
		return this.pkPoints;
	}

	public void setPkPoints(int points, boolean add) {
		if(add)
			this.pkPoints += points;
		else
			this.pkPoints = points;
	}
	
	public int getDungeoneeringTokens() {
		return dungTokens;
	}

	public void setDungeoneeringTokens(int dungTokens, boolean add) {
		if(add)
			this.dungTokens += dungTokens;
		else
			this.dungTokens = dungTokens;
	}
	
	public int getVotingPoints() {
		return votingPoints;
	}
	
	public void setVotingPoints(int votingPoints) {
		this.votingPoints = votingPoints;
	}
	
	public void incrementVotingPoints() {
		this.votingPoints++;
	}
	
	public void incrementVotingPoints(int amt) {
		this.votingPoints += amt;
	}
	
	public void setVotingPoints(int points, boolean add) {
		if(add)
			this.votingPoints += points;
		else
			this.votingPoints = points;
	}
	
	public int getAchievementPoints() {
		return achievementPoints;
	}
	
	public void setAchievementPoints(int points, boolean add) {
		if(add)
			this.achievementPoints += points;
		else
			this.achievementPoints = points;
	}
}
