package com.arlania.world.content.combat.pvp;

import com.arlania.util.Misc;
import com.arlania.world.World;
import com.arlania.world.entity.impl.player.Player;
public abstract class Killstreak {
	public static void kill(Player player, Player other, int killstreak) {
		double pkPoints = player.getWildernessLevel() + (player.getWildernessLevel() / 6) + Misc.random(40, 70);
		int otherKillstreak = other.getPlayerKillingAttributes().getPlayerKillStreak();
		if (otherKillstreak >= 3) {
			pkPoints += 5;
			World.sendMessage("<icon=2><shad=ff0000>" + other.getUsername() + "'s killstreak of "
					+ other.getPlayerKillingAttributes().getPlayerKillStreak() + " has been ended by "
					+ player.getUsername() + "!");
			other.getPlayerKillingAttributes().setPlayerKillStreak(0);
		}
		switch (killstreak) {
		case 3:
			pkPoints += 2;
			World.sendMessage("<icon=2><shad=ff0000>" + player.getUsername() + " is currently on a "
					+ killstreak + " killstreak!");
			break;
		case 5:
			pkPoints += 3;
			World.sendMessage("<icon=2><shad=ff0000>" + player.getUsername() + " is currently on a "
					+ killstreak + " killstreak!");
			break;
		case 10:
			pkPoints += 5;
			World.sendMessage("<icon=2><shad=ff0000>" + player.getUsername() + " is currently on a "
					+ killstreak + " killstreak!");
			break;
		}
		/*if(WellOfGoodness.isActive("pkp")) {
			pkPoints *= WellOfGoodness.BONUSPKP;
		}*/
		player.getPointsHandler().setPkPoints((int) pkPoints, true);
		player.getPacketSender().sendMessage("You've received " + pkPoints + " PK Points!");
		player.getPointsHandler().refreshPanel();
	}
}
