package com.arlania.world.content.skill.impl.crafting;

import com.arlania.engine.task.Task;
import com.arlania.engine.task.TaskManager;
import com.arlania.model.Animation;
import com.arlania.model.Skill;
import com.arlania.model.definitions.ItemDefinition;
import com.arlania.model.input.impl.EnterAmountOfTipsToMake;
import com.arlania.world.content.Achievements;
import com.arlania.world.content.Achievements.AchievementData;
import com.arlania.world.content.PlayerPanel;
import com.arlania.world.entity.impl.player.Player;

public class Gems2 {

	enum GEM_DATA {
		
		  OPAL(1609, 45,12, 13, 1, new Animation(886)),
		  JADE(1611, 9187,12, 16, 3, new Animation(886)),
		  RED_TOPAZ(1613, 9188,12, 20, 5, new Animation(887)),
		  SAPPHIRE(1607, 9189,12, 27, 8, new Animation(888)),
		  EMERALD(1605, 9190,12, 34, 12, new Animation(889)),
		  RUBY(1603, 9191,12, 43, 16, new Animation(892)),
		  DIAMOND(1601, 9192,12, 55, 20, new Animation(886)),
		  DRAGONSTONE(1615, 9193,12, 67, 25, new Animation(885)),
		  ONYX(6573, 9194,12, 75, 40, new Animation(885));
		
		GEM_DATA(int uncutGem, int cutGem, int amount, int levelReq, int xpReward, Animation animation) {
			this.uncutGem = uncutGem;
			this.cutGem = cutGem;
			this.amount = amount;
			this.levelReq = levelReq;
			this.xpReward = xpReward;
			this.animation = animation;
		}
		
		private int uncutGem, cutGem, levelReq, xpReward, amount;
		private Animation animation;

		public int getUncutGem() {
			return uncutGem;
		}

		public int getCutGem() {
			return cutGem;
		}

		public int getLevelReq() {
			return levelReq;
		}

		public int getXpReward() {
			return xpReward;
		}

		public Animation getAnimation() {
			return animation;
		}
		
		public int getAmount() {
			return amount;
		}
		
		public static GEM_DATA forUncutGem(int uncutGem) {
			for(GEM_DATA data : GEM_DATA.values()) {
				if(data.getUncutGem() == uncutGem)
					return data;
			}
			return null;
		}
	}
	
	public static void selectionInterface(Player player, int gem) {
		player.getPacketSender().sendInterfaceRemoval();
		GEM_DATA data = GEM_DATA.forUncutGem(gem);
		if(data == null) {
			return;
		}
		if (player.getSkillManager().getMaxLevel(Skill.CRAFTING) < data.getLevelReq()) {
			player.getPacketSender().sendMessage("You need a Crafting level of atleast "+ data.getLevelReq() +" to craft this gem.");
			return;
		}
		player.setSelectedSkillingItem(gem);
		player.setInputHandling(new EnterAmountOfTipsToMake());
		player.getPacketSender().sendString(2799, ItemDefinition.forId(gem).getName()).sendInterfaceModel(1746, gem, 150).sendChatboxInterface(4429);
		player.getPacketSender().sendString(2800, "How many would you like to craft?");
	}

	public static void cutGem(final Player player, final int amount, final int uncutGem) {
		player.getPacketSender().sendInterfaceRemoval();
		player.getSkillManager().stopSkilling();
		final GEM_DATA data = GEM_DATA.forUncutGem(uncutGem);
		if(data == null)
			return;
		player.setCurrentTask(new Task(2, player, true) {
			int amountCut = 0;
			@Override
			public void execute() {
				if(!player.getInventory().contains(uncutGem)) {
					stop();
					return;
				}
				player.performAnimation(data.getAnimation());
				player.getInventory().delete(uncutGem, 1);
				player.getInventory().add(data.getCutGem(), data.getAmount());
				if(data == GEM_DATA.DIAMOND) {
					Achievements.doProgress(player, AchievementData.CRAFT_1000_DIAMOND_GEMS);
				} else if(data == GEM_DATA.ONYX) {
					Achievements.finishAchievement(player, AchievementData.CUT_AN_ONYX_STONE);
				}
				player.getSkillManager().addExperience(Skill.CRAFTING, data.getXpReward());
				player.setSkillShrine(player.getSkillShrine() + 1);
				//player.sendMessage("You now have @gre@" + player.getSkillShrine() + "@bla@ Skilling Points!");
				PlayerPanel.refreshPanel(player);
				amountCut++;
				if(amountCut >= amount)
					stop();
			}
		});
		TaskManager.submit(player.getCurrentTask());
	}

}
