package com.arlania.world.content.skill.impl.dungeoneering.rework.floors;

public enum Door {

    NORTH,
    EAST,
    SOUTH,
    WEST

}
