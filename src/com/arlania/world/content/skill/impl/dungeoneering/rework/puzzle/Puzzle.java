package com.arlania.world.content.skill.impl.dungeoneering.rework.puzzle;

import com.arlania.world.entity.impl.player.Player;

public interface Puzzle {

    boolean attemptPuzzle(Player player);

    void ifPuzzleWasSuccessful(Player player);

}
