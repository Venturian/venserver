package com.arlania.world.content.skill.impl.dungeoneering.rework.floors;

import com.arlania.model.container.impl.Shop;
import com.arlania.world.content.skill.impl.dungeoneering.rework.floors.roomImpl.DefaultRoom;
import com.arlania.world.entity.impl.player.Player;

import java.util.Iterator;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public abstract class DungeonFloor implements Iterable<Player> {

    protected DungeonRoom startingRoom;
    protected DungeonRoom[] rooms;
    protected Player[] players;
    protected DungeonShop dungeonShop;

    public DungeonFloor(final int roomCount) {
        this.startingRoom = new DefaultRoom();
        this.rooms = new DungeonRoom[roomCount];
        this.players = new Player[4];
        this.dungeonShop = new DungeonShop(Shop.ShopManager.getShop(45));
    }

    public void sendFloorMessage(String message){
        for(Player p : players){
            p.getPacketSender().sendMessage(message);
        }
    }

    public DungeonShop getDungeonShop() {
        return dungeonShop;
    }

    public DungeonRoom[] getRooms() {
        return rooms;
    }

    public final Stream<Player> stream(){
        return StreamSupport.stream(this.spliterator(), false);
    }

    public abstract void buildFloorLayout();

    @Override
    public final Iterator<Player> iterator() {
        return new FloorIterator(this.players);
    }
}
