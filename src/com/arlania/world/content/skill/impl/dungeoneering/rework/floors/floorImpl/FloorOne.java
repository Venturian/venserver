package com.arlania.world.content.skill.impl.dungeoneering.rework.floors.floorImpl;

import com.arlania.world.content.skill.impl.dungeoneering.rework.floors.DungeonFloor;
import com.arlania.world.content.skill.impl.dungeoneering.rework.floors.roomImpl.BossRoom;

public class FloorOne extends DungeonFloor {

    public FloorOne() {
        super(1);
    }

    @Override
    public void buildFloorLayout() {
        rooms[0] = new BossRoom();
    }
}
