package com.arlania.world.content.skill.impl.dungeoneering.rework.floors.roomImpl;

import com.arlania.model.Position;
import com.arlania.world.content.skill.impl.dungeoneering.rework.floors.DungeonRoom;

public class BossRoom extends DungeonRoom {
    @Override
    public void buildGridLayout() {
        roomGrid.placeNPC(45, new Position(3545, 456));
    }
}
