package com.arlania.world.content.skill.impl.dungeoneering.rework.floors;

import com.arlania.world.content.skill.impl.dungeoneering.rework.map.RoomGrid;
import com.arlania.world.content.skill.impl.dungeoneering.rework.puzzle.Puzzle;
import com.arlania.world.entity.impl.player.Player;

public abstract class DungeonRoom implements Puzzle {

    private DungeonRoom northRoom;
    private DungeonRoom eastRoom;
    private DungeonRoom southRoom;
    private DungeonRoom westRoom;

    protected RoomGrid roomGrid;

    protected Puzzle puzzle;

    public final boolean attemptDoor(Door door) {
        switch (door) {
            case NORTH:
                return northRoom != null;
            case EAST:
                return eastRoom != null;
            case SOUTH:
                return southRoom != null;
            case WEST:
                return westRoom != null;
        }
        return false;
    }

    public void openDoor(Player player, Door door) {
        if (attemptDoor(door)) {
            switch (door) {
                case NORTH:
                    northRoom.ifPuzzleWasSuccessful(player);
                    break;
                case EAST:
                    eastRoom.ifPuzzleWasSuccessful(player);
                    break;
                case SOUTH:
                    southRoom.ifPuzzleWasSuccessful(player);
                    break;
                case WEST:
                    westRoom.ifPuzzleWasSuccessful(player);
                    break;
            }
        }
    }

    public void setPuzzle(Puzzle puzzle){
        this.puzzle = puzzle;
    }

    public void setNorthRoom(DungeonRoom northRoom) {
        this.northRoom = northRoom;
    }

    public void setEastRoom(DungeonRoom eastRoom) {
        this.eastRoom = eastRoom;
    }

    public void setSouthRoom(DungeonRoom southRoom) {
        this.southRoom = southRoom;
    }

    public void setWestRoom(DungeonRoom westRoom) {
        this.westRoom = westRoom;
    }

    public abstract void buildGridLayout();

    @Override
    public boolean attemptPuzzle(Player player) {
        return puzzle.attemptPuzzle(player);
    }

    @Override
    public void ifPuzzleWasSuccessful(Player player) {
        puzzle.ifPuzzleWasSuccessful(player);
    }
}
