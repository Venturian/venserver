package com.arlania.world.content.skill.impl.dungeoneering.rework;

public class DungeoneeringSettings {

    public static final int FORM_PARTY_INTERFACE = 27224;
    public static final int PARTY_INTERFACE = 26224;
    public static final int DUNGEONEERING_GATESTONE_ID = 17489;

}
