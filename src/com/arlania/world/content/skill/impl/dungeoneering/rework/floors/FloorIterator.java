package com.arlania.world.content.skill.impl.dungeoneering.rework.floors;

import com.arlania.world.entity.impl.player.Player;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class FloorIterator implements Iterator<Player> {

    private Player[] players;
    private int currentPosition;

    public FloorIterator(Player[] rooms) {
        this.players = rooms;
        this.currentPosition = 0;
    }

    @Override
    public boolean hasNext() {
        return this.currentPosition < players.length;
    }

    @Override
    public Player next() {
        if(!hasNext()){
            throw new NoSuchElementException("Floor Iterator no more elements");
        }
        return this.players[this.currentPosition++];
    }
}
