package com.arlania.world.content.skill.impl.dungeoneering.rework.floors;

import com.arlania.model.Item;
import com.arlania.model.container.impl.Shop;

public class DungeonShop {

    private final Item[] shopItems;
    private final Shop shop;

    public DungeonShop(Shop shop) {
        this.shop = new Shop(null, 45, "Smugglers store", new Item(995), shop.getItems());
        this.shopItems = shop.getItems();
    }

    public void setShopItems(Item[] shopItems) {
        this.shop.setItems(shopItems);
        this.shop.refreshItems();
    }

    public void resetShopToDefaultItems(){
        this.shop.setItems(this.shopItems);
        this.shop.refreshItems();
    }

}
