package com.arlania.world.content.skill.impl.dungeoneering.rework.puzzle.impl;

import com.arlania.model.Position;
import com.arlania.world.content.dialogue.Dialogue;
import com.arlania.world.content.dialogue.DialogueExpression;
import com.arlania.world.content.dialogue.DialogueType;
import com.arlania.world.content.skill.impl.dungeoneering.rework.puzzle.Puzzle;
import com.arlania.world.content.transportation.TeleportHandler;
import com.arlania.world.content.transportation.TeleportType;
import com.arlania.world.entity.impl.player.Player;

public class DummyPuzzle implements Puzzle {

    private Position position;

    @Override
    public boolean attemptPuzzle(Player player) {
        final boolean[] puzzleSuccessfull = {false};
        player.setDialogueActionId(1);
        player.setDialogue(new Dialogue() {
            @Override
            public DialogueType type() {
                return DialogueType.OPTION;
            }

            @Override
            public DialogueExpression animation() {
                return null;
            }

            @Override
            public String[] dialogue() {
                return new String[]{"3"};
            }

            @Override
            public void specialAction() {
                switch (player.getDialogueActionId()){
                    case 1:
                        puzzleSuccessfull[0] = true;
                        break;
                }
            }
        });

        return puzzleSuccessfull[0];
    }

    @Override
    public void ifPuzzleWasSuccessful(Player player) {
        if(attemptPuzzle(player)){
            TeleportHandler.teleportPlayer(player, position, TeleportType.NORMAL);
        }
    }
}
