package com.arlania.world.content.skill.impl.dungeoneering.rework.map;

import com.arlania.model.GameObject;
import com.arlania.model.Position;
import com.arlania.world.entity.Entity;
import com.arlania.world.entity.impl.npc.NPC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public final class RoomGrid implements Iterable<Entity> {

    private List<Entity> entities;

    public RoomGrid() {
        this.entities = new ArrayList<>();
    }

    public void placeObject(final int objectId, Position position){
        entities.add(new GameObject(objectId, position));
    }

    public void placeNPC(final int npcId, Position position){
        entities.add(new NPC(npcId, position));
    }

    public void removeEntity(Position position){
        stream().filter(en -> en.getPosition().getX() == position.getX() && en.getPosition().getY() == position.getY()
        && en.getPosition().getZ() == position.getZ()).findFirst().ifPresent(entities::remove);
    }

    public Stream<Entity> stream(){
        return entities.stream();
    }

    @Override
    public Iterator<Entity> iterator() {
        return entities.iterator();
    }
}
