package com.arlania.world.content.skill.impl.fletching;

public enum ArrowData {
	
		HEADLESS(52, 314, 53, 15, 1),
		BRONZE(53, 39, 882, 19, 1),
		IRON(53, 40, 884, 37, 15),
		STEEL(53, 41, 886, 75, 30),
		MITHRIL(53, 42, 888, 113, 45),
		ADAMANT(53, 43, 890, 150, 60),
		RUNE(53, 44, 892, 188, 75),
		OPAL_BOLTS(877, 160, 24, 879),
		JADE_BOLTS(9139, 9187, 36, 9335),
		RED_TOPAZ_BOLTS(9141, 9188, 48, 9336),
		SAPPHIRE_BOLTS(9142, 9189, 72, 9337),
		EMERALD_BOLTS(9142, 9190, 83, 9338),
		RUBY_BOLTS(9143, 9191, 95, 9339),
		DIAMOND_BOLTS(9143, 9192, 105, 9340),
		DRAGON_BOLTS(9144, 9193, 123, 9341),
		ONYX_BOLTS(9144, 9194, 141, 9342);

		public int item1, item2, outcome, xp, levelReq;
		ArrowData(int item1, int item2, int outcome, int xp, int levelReq) {
			this.item1 = item1;
			this.item2 = item2;
			this.outcome = outcome;
			this.xp = xp;
			this.levelReq = levelReq;
		}
		
		ArrowData(int item1, int item2, int xp, int outcome) {
			this.item1 = item1;
			this.item2 = item2;
			this.outcome = outcome;
			this.xp = xp;
		}
		public int getItem1() {
			return item1;
		}

		public int getItem2() {
			return item2;
		}

		public int getOutcome() {
			return outcome;
		}

		public int getXp() {
			return xp;
		}

		public int getLevelReq() {
			return levelReq;
		}

		public static ArrowData forArrow(int id) {
			for (ArrowData ar : ArrowData.values()) {
				if (ar.getItem2() == id) {
					return ar;
				}
			}
			return null;
		}
}
