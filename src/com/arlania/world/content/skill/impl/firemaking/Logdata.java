package com.arlania.world.content.skill.impl.firemaking;

import com.arlania.world.entity.impl.player.Player;


public class Logdata {

	public enum logData {
		
		LOG(1511, 1, 40, 130),
		ACHEY(2862, 1, 40, 230),
		OAK(1521, 15, 60, 240),
		WILLOW(1519, 30, 90, 295),
		TEAK(6333, 35, 105, 45),
		ARCTIC_PINE(10810, 42, 125, 245),
		MAPLE(1517, 45, 135, 345),
		MAHOGANY(6332, 50, 158, 45),
		EUCALYPTUS(12581, 58, 194, 45),
		YEW(1515, 60, 202, 400),
		MAGIC(1513, 75, 303, 500);
		
		private int logId, level, burnTime;
		private int xp;
		
		logData(int logId, int level, int xp, int burnTime) {
			this.logId = logId;
			this.level = level;
			this.xp = xp;
			this.burnTime = burnTime;
		}
		
		public int getLogId() {
			return logId;
		}
		
		public int getLevel() {
			return level;
		}
		
		public int getXp() {
			return xp;
		}		
		
		public int getBurnTime() {
			return this.burnTime;
		}
	}

	public static logData getLogData(Player p, int log) {
		for (final Logdata.logData l : Logdata.logData.values()) {
			if(log == l.getLogId() || log == -1 && p.getInventory().contains(l.getLogId())) {
				return l;
			}
		}
		return null;
	}

}