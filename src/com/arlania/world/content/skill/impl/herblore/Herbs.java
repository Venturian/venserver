package com.arlania.world.content.skill.impl.herblore;

public enum Herbs {
	
	GUAM(199, 249, 1, 3),
	MARRENTILL(201, 251, 9, 5),
	TARROMIN(203, 253, 5, 4),
	HARRALANDER(205, 255, 20, 6),
	RANARR(207, 257, 25, 7),
	TOADFLAX(3049, 2998, 30, 8),
	SPIRITWEED(12174, 12172, 35, 8),
	IRIT(209, 259, 40, 9),
	WERGALI(14836, 14854, 30, 9),
	AVANTOE(211, 261, 48, 10),
	KWUARM(213, 263, 54, 10),
	SNAPDRAGON(3051, 3000, 59, 11),
	CADANTINE(215, 265, 65, 12),
	LANTADYME(2485, 2481, 67, 13),
	DWARFWEED(217, 267, 70, 14),
	TORSTOL(219, 269, 75, 15);
	
	private final int grimyHerb, cleanHerb, levelReq, cleaningExp;
	
	Herbs(int grimyHerb, int cleanHerb, int levelReq, int cleaningExp) {
		this.grimyHerb = grimyHerb;
		this.cleanHerb = cleanHerb;
		this.levelReq = levelReq;
		this.cleaningExp = cleaningExp;
	}
	
	public int getGrimyHerb() {
		return grimyHerb;
	}
	
	public int getCleanHerb() {
		return cleanHerb;
	}
	
	public int getLevelReq() {
		return levelReq;
	}
	
	public int getExp() {
		return cleaningExp;
	}
	
	public static Herbs forId(int herbId){
		for(Herbs herb : Herbs.values()) {
			if (herb.getGrimyHerb() == herbId) {
				return herb;
			}
		}
		return null;
	}
	
}