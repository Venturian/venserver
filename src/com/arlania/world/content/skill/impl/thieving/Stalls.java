package com.arlania.world.content.skill.impl.thieving;

import com.arlania.model.Animation;
import com.arlania.model.CombatIcon;
import com.arlania.model.Hit;
import com.arlania.model.Hitmask;
import com.arlania.model.Item;
import com.arlania.model.Locations;
import com.arlania.model.PlayerRights;
import com.arlania.model.Position;
import com.arlania.model.Projectile;
import com.arlania.model.Skill;
import com.arlania.model.Locations.Location;
import com.arlania.model.definitions.ItemDefinition;
import com.arlania.util.Misc;
import com.arlania.util.RandomUtility;
import com.arlania.util.ShutdownHook;
import com.arlania.world.content.Achievements;
import com.arlania.world.content.Achievements.AchievementData;
import com.arlania.world.content.ExperienceLamps;
import com.arlania.world.content.transportation.TeleportHandler;
import com.arlania.world.entity.impl.player.Player;

public class Stalls {

	public static void stealFromStall(Player player, int lvlreq, int xp, int reward, String message) {
		if(player.getInventory().getFreeSlots() < 1) {
			player.getPacketSender().sendMessage("You need some more inventory space to do this.");
			return;
		}
		if (player.getCombatBuilder().isBeingAttacked()) {
			player.getPacketSender().sendMessage("You must wait a few seconds after being out of combat before doing this.");
			return;
		}
		if(!player.getClickDelay().elapsed(2000))
			return;
		if(player.getSkillManager().getMaxLevel(Skill.THIEVING) < lvlreq) {
			player.getPacketSender().sendMessage("You need a Thieving level of at least " + lvlreq + " to steal from this stall.");
			return;
		}
			if (RandomUtility.RANDOM.nextInt(5) == 2) {
				Hit dmg;
				if(player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION) < 20) {
					dmg = new Hit(5, Hitmask.RED, CombatIcon.NONE);
				} else if (player.getSkillManager().getCurrentLevel(Skill.CONSTITUTION) < 40) {
					dmg = new Hit(8, Hitmask.RED, CombatIcon.NONE);
				} else {
					dmg = new Hit(15, Hitmask.RED, CombatIcon.NONE);
				}
				
				player.decrementHealth(dmg);
				player.dealDamage(dmg);
				player.getCombatBuilder().addDamage(player, dmg.getDamage());
				player.getPacketSender().sendMessage("You failed to steal from the stall and got hit.");
				return;
			} 
		player.performAnimation(new Animation(881));
		player.getPacketSender().sendMessage(message);
		player.getPacketSender().sendInterfaceRemoval();
		player.getSkillManager().addExperience(Skill.THIEVING, xp);
		player.getClickDelay().reset();
		if (player.isDonator()) {
			player.getInventory().add(995, 500);
		}
		if (player.isSuperDonator()) {
			player.getInventory().add(995, 1000);
		}
		if (player.isExtremeDonator()) {
			player.getInventory().add(995, 2000);
		}
		if (player.isLegendaryDonator()) {
			player.getInventory().add(995, 3000);
		}
		if (player.isUberDonator()) {
			player.getInventory().add(995, 4000);
		}
		
		if (player.getLocation() == Location.DONATOR_ZONE) {
			Item item = new Item(reward);
			int value = item.getDefinition().getValue();		
			player.getInventory().add(995, value);
		} else {
			player.getInventory().add(reward, 1);

		}
		
		player.getSkillManager().stopSkilling();
		if(reward == 15009)
			Achievements.finishAchievement(player, AchievementData.STEAL_A_RING);
		else if(reward == 11998) {
			Achievements.doProgress(player, AchievementData.STEAL_140_SCIMITARS);
			Achievements.doProgress(player, AchievementData.STEAL_5000_SCIMITARS);
		}
	}
	

}
