package com.arlania.world.content;

import com.arlania.GameLoader;
import com.arlania.util.Misc;
import com.arlania.world.World;
import com.arlania.world.content.minigames.impl.Nomad;
import com.arlania.world.content.minigames.impl.RecipeForDisaster;
import com.arlania.world.entity.impl.player.Player;

public class PlayerPanel {

	public static final String LINE_START = "   @whi@> ";

	public static void refreshPanel(Player player) {

		int counter = 39159;
		player.getPacketSender().sendString(counter++, "<img=41>@fz2@ Quick actions:");
		player.getPacketSender().sendString(counter++, LINE_START.replace(">", "") + "@red@[VIEW] @fz1@Staff online");
		player.getPacketSender().sendString(counter++, LINE_START.replace(">", "") + "@red@[VIEW] @fz1@Scoreboard");
		player.getPacketSender().sendString(counter++, LINE_START.replace(">", "") + "@red@[VIEW] @fz1@Monster Killcounts");
		player.getPacketSender().sendString(counter++, LINE_START.replace(">", "") + "@red@[VIEW] @fz1@Monster Drops Log");

		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");

		player.getPacketSender().sendString(counter++, "<img=38>@fz2@ Server statistics:");
		//player.getPacketSender().sendString(counter++, LINE_START + "@or1@Players online: @de0@" + World.getPlayers().size());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Evil Tree: @de0@"+(EvilTrees.getLocation() != null ? EvilTrees.getLocation().playerPanelFrame : "N/A"));
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Well of Goodwill: @de0@"+(WellOfGoodwill.isActive() ? WellOfGoodwill.getMinutesRemaining() + " mins" : "N/A"));
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Crashed Star: @de0@"+(ShootingStar.getLocation() != null ?ShootingStar.getLocation().playerPanelFrame : "N/A"));
		//player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Bonus: @de0@"+GameLoader.getSpecialDay());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Bonus: @de0@ None active");
		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");

		player.getPacketSender().sendString(counter++, "<img=39>@fz2@ Player statistics:");
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Server Time: @de0@"+Misc.getCurrentServerTime());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Time Played: @de0@"+Misc.getHoursPlayed((player.getTotalPlayTime() + player.getRecordedLogin().elapsed())));
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Username: @de0@"+player.getUsername());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Rank: @de0@"+player.getRights().toString());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Donated: @de0@"+player.getAmountDonated());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Exp Lock: @de0@"+(player.experienceLocked() ? "Locked" : "Unlocked"));
		
		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");
		
		player.getPacketSender().sendString(counter++, "<img=40>@fz2@ Player points:");
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Loyalty Points:@de0@ "+player.getPointsHandler().getLoyaltyPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Prestige Points:@de0@ "+player.getPointsHandler().getPrestigePoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Trivia Points:@de0@ "+player.getPointsHandler().getTriviaPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Voting Points:@de0@ "+player.getPointsHandler().getVotingPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Donation Points:@de0@ "+player.getPointsHandler().getDonationPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Commendations:@de0@ "+player.getPointsHandler().getCommendations());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Dung. Tokens:@de0@ "+player.getPointsHandler().getDungeoneeringTokens());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Boss Points:@de0@ "+player.getBossPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Skilling Points:@de0@ "+player.getSkillShrine());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Slayer Points:@de0@ "+player.getPointsHandler().getSlayerPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Pk Points:@de0@ "+player.getPointsHandler().getPkPoints());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Wilderness Killstreak:@de0@ "+player.getPlayerKillingAttributes().getPlayerKillStreak());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Wilderness Kills:@de0@ "+player.getPlayerKillingAttributes().getPlayerKills());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Wilderness Deaths:@de0@ "+player.getPlayerKillingAttributes().getPlayerDeaths());
//		player.getPacketSender().sendString(counter++, LINE_START + "@or1@Arena Victories:@yel@ "+player.getDueling());
//		player.getPacketSender().sendString(counter++, LINE_START + "@or1@Arena Losses:@yel@ "+player.getPointsHandler().getLoyaltyPoints());

		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");

		player.getPacketSender().sendString(counter++, "<img=37>@fz2@ Slayer info:");
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Master: @de0@"+player.getSlayer().getSlayerMaster());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Task: @de0@"+player.getSlayer().getSlayerTask());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Task Amount: @de0@"+player.getSlayer().getAmountToSlay());
		player.getPacketSender().sendString(counter++, LINE_START + "@fz1@Task Streak: @de0@"+player.getSlayer().getTaskStreak());

		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");

		player.getPacketSender().sendString(counter++, "<img=39>@fz2@ Quests:");
		player.getPacketSender().sendString(counter++,
				LINE_START + RecipeForDisaster.getQuestTabPrefix(player) + "Recipe For Disaster ");
		player.getPacketSender().sendString(counter++,
				LINE_START + Nomad.getQuestTabPrefix(player) + "Nomad's Requiem ");

		player.getPacketSender().sendString(counter++, "");
		player.getPacketSender().sendString(counter++, "");
		player.getPacketSender().sendString(counter++, "");
		player.getPacketSender().sendString(counter++, "<img=43>@bla@@whi@");
		player.getPacketSender().sendString(counter++, "");
		player.getPacketSender().sendString(counter++, "");
		player.getPacketSender().sendString(counter++, "");
		/**
		 * General info
		 *
		 * player.getPacketSender().sendString(39159, "@or3@ - @whi@ General
		 * Information");
		 * 
		 * if(ShootingStar.CRASHED_STAR == null) {
		 * player.getPacketSender().sendString(26623, "@or2@Crashed
		 * star: @de0@N/A"); } else { player.getPacketSender().sendString(26623,
		 * "@or2@Crashed
		 * star: @de0@"+ShootingStar.CRASHED_STAR.getStarLocation().playerPanelFrame+"");
		 * }
		 * 
		 * if(EvilTrees.SPAWNED_TREE == null) {
		 * player.getPacketSender().sendString(26625, "@or2@Evil
		 * Tree: @de0@N/A"); } else { player.getPacketSender().sendString(26625,
		 * "@or2@Evil
		 * Tree: @de0@"+EvilTrees.SPAWNED_TREE.getTreeLocation().playerPanelFrame+"");
		 * }
		 * 
		 * if(GameLoader.getSpecialDay() != null) {
		 * player.getPacketSender().sendString(26626, "@or2@Bonus: @de0@"+
		 * GameLoader.getSpecialDay()); } else { if(GameLoader.getSpecialDay()
		 * != null) { return; } }
		 * 
		 * if(WellOfGoodwill.isActive()) {
		 * player.getPacketSender().sendString(26622, "@or2@Well of
		 * Goodwill: @de0@Active"); } else {
		 * player.getPacketSender().sendString(26622, "@or2@Well of
		 * Goodwill: @de0@N/A"); }
		 * 
		 * /** Account info
		 * 
		 * player.getPacketSender().sendString(39165, "@or3@ - @whi@ Account
		 * Information"); player.getPacketSender().sendString(39167,
		 * "@or2@Username: @yel@"+player.getUsername());
		 * player.getPacketSender().sendString(39168,
		 * "@or2@Claimed: @yel@$"+player.getAmountDonated());
		 * player.getPacketSender().sendString(39169,
		 * "@or2@Rank: @yel@"+Misc.formatText(player.getRights().toString().toLowerCase()));
		 * player.getPacketSender().sendString(39170,
		 * "@or2@Email: @yel@"+(player.getEmailAddress() == null ||
		 * player.getEmailAddress().equals("null") ? "-" :
		 * player.getEmailAddress()));
		 * player.getPacketSender().sendString(39171,
		 * "@or2@Music: @yel@"+(player.musicActive() ? "On" : "Off")+"");
		 * player.getPacketSender().sendString(39172,
		 * "@or2@Sounds: @yel@"+(player.soundsActive() ? "On" : "Off")+"");
		 * player.getPacketSender().sendString(26721, "@or2@Exp
		 * Lock: @de0@"+(player.experienceLocked() ? "Locked" : "Unlocked")+"");
		 * 
		 * /** Points
		 * 
		 * player.getPacketSender().sendString(39174, "@or3@ - @whi@
		 * Statistics"); player.getPointsHandler().refreshPanel();
		 * 
		 * /** Slayer
		 * 
		 * player.getPacketSender().sendString(39189, "@or3@ - @whi@ Slayer");
		 * player.getPacketSender().sendString(39190, "@or2@Open Kills
		 * Tracker"); player.getPacketSender().sendString(39191, "@or2@Open Drop
		 * Log"); player.getPacketSender().sendString(26716,
		 * "@or2@Master: @de0@"+Misc.formatText(player.getSlayer().getSlayerMaster().toString().toLowerCase().replaceAll("_",
		 * " "))); if(player.getSlayer().getSlayerTask() == SlayerTasks.NO_TASK)
		 * player.getPacketSender().sendString(26717,
		 * "@or2@Task: @de0@"+Misc.formatText(player.getSlayer().getSlayerTask().toString().toLowerCase().replaceAll("_",
		 * " "))+""); else player.getPacketSender().sendString(26717,
		 * "@or2@Task: @de0@"+Misc.formatText(player.getSlayer().getSlayerTask().toString().toLowerCase().replaceAll("_",
		 * " "))+"s"); player.getPacketSender().sendString(26718, "@or2@Task
		 * Streak: @de0@"+player.getSlayer().getTaskStreak()+"");
		 * player.getPacketSender().sendString(26719, "@or2@Task
		 * Amount: @de0@"+player.getSlayer().getAmountToSlay()+"");
		 * if(player.getSlayer().getDuoPartner() != null)
		 * player.getPacketSender().sendString(26720, "@or2@Duo
		 * Partner: @de0@"+player.getSlayer().getDuoPartner()+""); else
		 * player.getPacketSender().sendString(26720, "@or2@Duo
		 * Partner: @de0@None");
		 * 
		 * /** Quests
		 * 
		 * player.getPacketSender().sendString(26722, "@or3@ - @whi@ Quests");
		 * player.getPacketSender().sendString(26723,
		 * RecipeForDisaster.getQuestTabPrefix(player) + "Recipe For Disaster");
		 * player.getPacketSender().sendString(26724,
		 * Nomad.getQuestTabPrefix(player) + "Nomad's Requeim");
		 * 
		 * /** Links
		 * 
		 * player.getPacketSender().sendString(39202, "@or3@ - @whi@ Links");
		 * player.getPacketSender().sendString(39203, "@or2@Forum");
		 * player.getPacketSender().sendString(39204, "@or2@Rules");
		 * player.getPacketSender().sendString(39205, "@or2@Store");
		 * player.getPacketSender().sendString(39206, "@or2@Vote");
		 * player.getPacketSender().sendString(39207, "@or2@Hiscores");
		 * player.getPacketSender().sendString(39208, "@or2@Report");
		 */
	}

}