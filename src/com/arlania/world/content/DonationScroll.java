package com.arlania.world.content;

import com.arlania.world.entity.impl.player.Player;

public class DonationScroll {
	private static final int fiveDolarScrollId = 1;
	private static final int tenDolarScrollId = 15;
	private static final int twentyFiveDolarScrollId = 5;
	private static final int fiftyDolarScrollId = 14;
	private static final int hundredDolarScrollId = 3;
	
	public static void claimPoints(Player player, int id){
		player.getInventory().delete(id, 1);
		int aantal = giveAmount(id);
		player.getPointsHandler().incrementDonationPoints(aantal);
		player.incrementAmountDonated(aantal);
		
		player.getPacketSender().sendMessage("You succesfully claimed your " + aantal + "$ scroll.");
		player.getPacketSender().sendMessage("Your donation points have been increased by " + aantal + "!");
		player.getPointsHandler().refreshPanel();
		PlayerPanel.refreshPanel(player);
	}
	
	private static int giveAmount(int id){
		switch(id){
		case fiveDolarScrollId:
			return 5;
		case tenDolarScrollId:
				return 10;
		case twentyFiveDolarScrollId:
			return 25;
		case fiftyDolarScrollId:
			return 50;
		case hundredDolarScrollId:
			return 100;
		default: return 0;
		}
		
		
	}
}
