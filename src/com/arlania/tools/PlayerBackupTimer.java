package com.arlania.tools;

import com.arlania.util.Stopwatch;

public class PlayerBackupTimer {
	private static final int TIME1 = 3600000;
	private static Stopwatch timer1 = new Stopwatch().reset();
	private static final int TIME2 = 86400000;
	private static Stopwatch timer2 = new Stopwatch().reset();
	private static final int TIME3 = 604800000;
	private static Stopwatch timer3 = new Stopwatch().reset();
	private static final int TIME4 = 604800000;
	private static Stopwatch timer4 = new Stopwatch().reset();
	private static int aantalWeken = 0;
	
	public static void startSequences() {
		sequence1();
		sequence2();
		sequence3();
		sequence4();
    }
	
	//elk uur
	private static void sequence1(){
		if(timer1.elapsed(TIME1)) {
			timer1.reset();
			Thread t = new Thread(new PlayerBackup("backups_hour"));
			t.start();
		}
	}
	
	//elke dag
	private static void sequence2(){
		if(timer2.elapsed(TIME2)) {
			timer2.reset();
			Thread t = new Thread(new PlayerBackup("backups_day"));
			t.start();
		}
	}
	
	//elke week
	private static void sequence3(){
		if(timer3.elapsed(TIME3)) {
			timer3.reset();
			Thread t = new Thread(new PlayerBackup("backups_week"));
			t.start();
		}
	}

	//elke maand
	private static void sequence4(){
		if(timer4.elapsed(TIME4)) {
			aantalWeken++;
			timer4.reset();
			if(aantalWeken == 4){
				aantalWeken = 0;
				Thread t = new Thread(new PlayerBackup("backups_month"));
				t.start();
			}
		}
	}
}
