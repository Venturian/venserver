package com.arlania.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;

import com.arlania.GameServer;

/**
 * Handles functionality to create a backup of the player files.
 *
 * @author Andys1814.
 */
public final class PlayerBackup implements Runnable{

	/**
	 * Represents the directory for which this class will backup the contents
	 * of.
	 */
	
	private String backupFolder;
	
	public PlayerBackup(String backupFolder) {
		this.backupFolder = backupFolder;
	}
	
	private File FROM_DIRECTORY;

	private File TO_DIRECTORY;

	public void runBackup() {
		GameServer.getLogger().info("backup in " + backupFolder);
		Path path = Paths.get("./data/saves/characters");
		FROM_DIRECTORY = path.toFile();
		Path path2 = Paths.get("./data/" + backupFolder);
		TO_DIRECTORY = path2.toFile();
		
		if (!FROM_DIRECTORY.exists()) {
			GameServer.getLogger().log(Level.SEVERE, "Source file not found.");
		}else {
			if (!TO_DIRECTORY.exists()) {
					TO_DIRECTORY.mkdir();
			}
			
			if(TO_DIRECTORY.exists()){
				try {
					for(File file : FROM_DIRECTORY.listFiles()){
						if(file.getName().substring(file.getName().length()-4, file.getName().length()).equals("json")){
							Files.copy(Paths.get("./data/saves/characters/" + file.getName()), Paths.get("./data/" + backupFolder + "/" + file.getName()), StandardCopyOption.REPLACE_EXISTING);
						}
					}
					
				} catch (IOException e) {
					GameServer.getLogger().log(Level.SEVERE, "Backup failed.", e);
					e.printStackTrace();
				}
			}else{
				GameServer.getLogger().log(Level.SEVERE, "Backup failed.");
			}
		}

	}

	@Override
	public void run() {
		runBackup();
		
	}
	

}
