package com.arlania.net.packet.impl.itemActions;

import com.arlania.model.container.impl.Inventory;
import com.arlania.model.definitions.ItemDefinition;
import com.arlania.net.packet.Packet;
import com.arlania.world.entity.impl.player.Player;

import javax.management.BadAttributeValueExpException;
import java.util.function.BiConsumer;

public class ItemAction {

    public static ItemAction readActionData(Packet packet) {
        final int interfaceId = packet.readShort();
        final int slotId = packet.readShort();
        final int itemId = packet.readShort();
        final int menuPosition = packet.readByte();
        return new ItemAction(interfaceId, slotId, menuPosition, itemId);
    }

    private int interfaceId;
    private int slotId;
    private int menuPosition;
    private ItemDefinition itemDefinition;

    private BiConsumer<Player, ItemAction> actionToPreform;

    private ItemAction(int interfaceId, int slotId, int menuPosition, int itemId) {
        this.interfaceId = interfaceId;
        this.slotId = slotId;
        this.menuPosition = menuPosition;
        this.itemDefinition = ItemDefinition.forId(itemId);
    }

    public boolean validate(Inventory inventory) {
        return inventory.get(this.slotId) != null && inventory.get(this.slotId).getId() == this.itemDefinition.getId();
    }

    public int getInterfaceId() {
        return interfaceId;
    }

    public int getSlotId() {
        return slotId;
    }

    public int getMenuPosition() {
        return menuPosition;
    }

    public ItemDefinition getItemDefinition() {
        return itemDefinition;
    }

    public void setActionToPreform(final int menuPosition, BiConsumer<Player, ItemAction> actionToPreform) throws BadAttributeValueExpException {
        if(menuPosition < 5){
            throw new BadAttributeValueExpException("Menu Position can't be less then 5!");
        }
        if (this.menuPosition == menuPosition) {
            this.actionToPreform = actionToPreform;
        }
    }

    public void doAction(Player player) {
        if (this.actionToPreform != null) {
            this.actionToPreform.accept(player, this);
        }
    }

}
