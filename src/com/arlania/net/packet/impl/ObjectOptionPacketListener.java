package com.arlania.net.packet.impl;

import com.arlania.net.packet.Packet;
import com.arlania.net.packet.PacketListener;
import com.arlania.world.entity.impl.player.Player;

/**
 * 
 * @author David (JavaGuy)
 * ObjectOptionPacketListener.java
 * Handles all actions for options >= 5
 * This handles objects, example bank booth options.
 *
 */

public class ObjectOptionPacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		final int objectId = packet.readShort();
		final int x = packet.readShort();
		final int y = packet.readShort();
		final int menuPosition = packet.readByte();
		
		switch(menuPosition) {
		
		
		default:
			if(player.getRights().isStaff()) {
				player.getPacketSender().sendMessage("Object ID: " + objectId + " Coords:( " + x + ", " + y + " )");
				player.getPacketSender().sendMessage("Menu Position: " + menuPosition);
			}
			break;
		}
		
	}

}
