package com.arlania.net.packet.impl;

import com.arlania.model.PlayerRights;
import com.arlania.net.packet.Packet;
import com.arlania.net.packet.PacketListener;
import com.arlania.world.World;
import com.arlania.world.entity.impl.player.Player;

//CALLED EVERY 3 MINUTES OF INACTIVITY

public class IdleLogoutPacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		if(player.getRights() == PlayerRights.OWNER || player.getRights() == PlayerRights.DEVELOPER || player.isModerator() || player.isAdministrator() || player.isSupport())
			return;
		if(player.logout() && (player.getCurrentTask() == null || !player.getCurrentTask().isRunning())) {
			World.getPlayers().remove(player);
		}
		player.setInactive(true);
	}
}
